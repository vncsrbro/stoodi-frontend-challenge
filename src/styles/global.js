import { createGlobalStyle } from "styled-components";
import { PRIMARY_FONT, PRIMARY_DARK } from "./theme";

export const GlobalStyle = createGlobalStyle`
	@import url('https://fonts.googleapis.com/css?family=Lato:400,700&display=swap');

	* {
		border: none;
		box-sizing: border-box;
		list-style: none;
		margin: 0;
		outline: none;
		padding: 0;
	}

	html,
	body {
		color: ${PRIMARY_DARK};
		font-family: ${PRIMARY_FONT};
		height: 100%;
	}

	a {
		text-decoration: none;
	}

	img {
		max-width: 100%;
	}
`;
