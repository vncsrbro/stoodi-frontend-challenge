/**
 * SIZES
 */

export const SIZES = {
  xs: "14px",
  sm: "16px",
  md: "18px",
  lg: "20px",
  xl: "24px"
};

/**
 * FONTS
 */

export const PRIMARY_FONT = "'Lato', sans-serif;";
