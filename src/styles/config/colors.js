/**
 * PRIMARY
 */
export const PRIMARY_LIGHT = "#4868EE";
export const PRIMARY = "#3153F5";
export const PRIMARY_DARK = "#1E2A54";

/**
 * AUXILIAR
 */

export const WARNING_LIGHT = "#ffded8";
export const WARNING = "#ff5e37";

export const SUCCESS_LIGHT = "#cef0e4";
export const SUCCESS = "#00bf28";

export const GREY = "#ebebeb";
export const GREY_DARK = "#9b9b9b";

/**
 * DEFAULT
 */
export const LIGHT = "#FFFFFF";
export const DARK = "#333333";
