import React, { Component } from "react";

import api from "../../../services/api";

import Question from "../../commons/Question";

export default class HomeTemplate extends Component {
  state = {
    questions: [],
    noQuestions: false
  };

  componentDidMount() {
    api
      .get("/")
      .then(response => {
        this.setState({ questions: [response.data] });
      })
      .catch(error => {
        this.setState({ noQuestions: true });
        return error;
      });
  }

  render() {
    const { questions, noQuestions } = this.state;
    return (
      <>
        {questions.map(question => (
          <Question
            key={question.exercise.exercise_id}
            questionID={question.exercise.exercise_id}
            questionInstitution={question.exercise.institution}
            questionTitle={question.exercise.exercise_text}
            questionOptions={question.exercise.alternatives}
          />
        ))}

        {noQuestions && "Nenhuma questão encontrada"}
      </>
    );
  }
}
