import React from "react";
import PropTypes from "prop-types";

import { StyledButton } from "./style";

const Button = ({ onClick, status, text }) => {
  return (
    <>
      <StyledButton onClick={onClick} type="submit" status={status}>
        {text}
      </StyledButton>
    </>
  );
};

Button.defaultProps = {
  onClick: () => {}
};

Button.propTypes = {
  onClick: PropTypes.func,
  status: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired
};

export default Button;
