import styled from "styled-components";
import {
  PRIMARY_FONT,
  LIGHT,
  PRIMARY,
  SIZES,
  PRIMARY_LIGHT,
  DEVICE,
  GREY,
  GREY_DARK
} from "../../../styles/theme";

const handleStatus = status => {
  switch (status) {
    case "disabled":
      return `
				background-color: ${GREY};
				color: ${GREY_DARK};
				pointer-events: none;
			`;

    case "active":
      return `
				background-color: ${PRIMARY};
				color: ${LIGHT};
			`;

    // skip default
  }
};

export const StyledButton = styled.button`
  align-items: center;

  border-radius: 20px;
  cursor: pointer;
  display: flex;
  font-family: ${PRIMARY_FONT};
  font-size: ${SIZES.xs};
  font-weight: bold;
  height: 40px;
  justify-content: center;
  text-transform: uppercase;
  transition: 300ms ease all;
  width: 190px;
  ${props => handleStatus(props.status)};

  &:hover {
    background-color: ${PRIMARY_LIGHT};
  }

  @media ${DEVICE.mobile} {
    width: 100%;
  }
`;
