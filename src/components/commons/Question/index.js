import React, { useState } from "react";
import PropTypes from "prop-types";
import renderHTML from "react-render-html";

import api from "../../../services/api";

import {
  QuestionWrapper,
  QuestionContent,
  QuestionHeader,
  QuestionInstitution,
  QuestionTitle,
  QuestionOptions,
  QuestionOptionsItem,
  QuestionOptionsItemLetter,
  QuestionFooter,
  QuestionFooterResponse,
  QuestionFooterAction
} from "./style";

import Button from "../Button/index";

const Question = ({
  questionID,
  questionInstitution,
  questionTitle,
  questionOptions
}) => {
  const responseMessages = {
    success: "<b>Resposta correta</b> Boa! Acertou em cheio.",
    error: "<b>Resposta incorreta</b> Que tal tentar novamente?"
  };

  const [buttonStatus, setButtonStatus] = useState("disabled");
  const [buttonText, setButtonText] = useState("Verificar resposta");
  const [correction, setCorrection] = useState();
  const [repeatQuestion, setRepeatQuestion] = useState();
  const [selectedOption, setSelectedOption] = useState("");
  const [validationMessage, setValidationMessage] = useState("");

  const handleOptionClick = option => {
    setSelectedOption(option);
    setButtonStatus("active");
  };

  const handleAnswerSubmission = () => {
    api
      .post("/", {
        exercise_id: questionID,
        choice: selectedOption
      })
      .then(response => {
        const {
          data: { is_correct }
        } = response;

        const currentSelected = document.querySelector(
          'input[name="answer"]:checked'
        );

        switch (is_correct) {
          case true:
            currentSelected.parentElement.parentElement.classList.add(
              "correct"
            );

            document.querySelector("button").setAttribute("disabled", "true");
            setButtonText("Próximo");
            setCorrection(true);
            setValidationMessage(responseMessages.success);
            break;

          case false:
            currentSelected.parentElement.parentElement.classList.add("wrong");
            setCorrection(false);
            setRepeatQuestion(true);
            setValidationMessage(responseMessages.error);

            break;

          // skip default
        }

        const optionsInputs = document.querySelectorAll('input[name="answer"]');
        Array.from(optionsInputs).map(option => {
          option.setAttribute("disabled", "true");
        });
      })
      .catch(error => {
        return error;
      });
  };

  const handleRepeatQuestion = () => {
    const currentSelected = document.querySelector(
      'input[name="answer"]:checked'
    );
    currentSelected.parentElement.parentElement.classList.remove(
      "wrong",
      "correct"
    );
    currentSelected.checked = false;

    const optionsInputs = document.querySelectorAll('input[name="answer"]');
    Array.from(optionsInputs).map(option => {
      option.removeAttribute("disabled");
    });

    setCorrection();
    setRepeatQuestion(false);
    setValidationMessage("");
    setButtonStatus("disabled");
  };

  return (
    <>
      <QuestionWrapper correction={correction}>
        <QuestionContent>
          <QuestionHeader>
            <QuestionInstitution>{questionInstitution}</QuestionInstitution>
            <QuestionTitle>{renderHTML(questionTitle)}</QuestionTitle>
          </QuestionHeader>

          <QuestionOptions>
            {questionOptions.map(option => (
              <QuestionOptionsItem key={option.letter}>
                <label htmlFor={option.letter}>
                  <input
                    onClick={() => handleOptionClick(option.letter)}
                    type="radio"
                    name="answer"
                    id={option.letter}
                  />
                  <QuestionOptionsItemLetter>
                    {option.letter}
                  </QuestionOptionsItemLetter>
                  {option.label}
                </label>
              </QuestionOptionsItem>
            ))}
          </QuestionOptions>

          <QuestionFooter>
            <QuestionFooterResponse>
              {renderHTML(validationMessage)}
            </QuestionFooterResponse>

            <QuestionFooterAction>
              {repeatQuestion ? (
                <Button
                  onClick={() => handleRepeatQuestion()}
                  text="Refazer"
                  status="active"
                />
              ) : (
                <Button
                  onClick={() => handleAnswerSubmission({ questionID })}
                  text={buttonText}
                  status={buttonStatus}
                />
              )}
            </QuestionFooterAction>
          </QuestionFooter>
        </QuestionContent>
      </QuestionWrapper>
    </>
  );
};

Question.propTypes = {
  questionID: PropTypes.number.isRequired,
  questionInstitution: PropTypes.string.isRequired,
  questionTitle: PropTypes.string.isRequired,
  questionOptions: PropTypes.instanceOf(Array).isRequired
};

export default Question;
