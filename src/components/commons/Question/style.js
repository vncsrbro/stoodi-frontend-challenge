import styled from "styled-components";
import {
  GREY,
  SIZES,
  PRIMARY,
  DEVICE,
  SUCCESS_LIGHT,
  WARNING_LIGHT,
  WARNING,
  SUCCESS
} from "../../../styles/theme";

const handleCorrectionStatus = status => {
  switch (status) {
    case true:
      return `
				footer {
					background-color: ${SUCCESS_LIGHT}
				}
			`;

    case false:
      return `
				footer {
					background-color: ${WARNING_LIGHT}
				}
			`;

    // skip default
  }
};

export const QuestionOptionsItem = styled.div`
  padding: 10px;

  label {
    align-items: center;
    display: flex;
  }

  input[type="radio"] {
    -webkit-appearance: inherit;
    border: 1px solid ${GREY};
    border-radius: 50%;
    height: 20px;
    margin: 0 10px 0 0;
    position: relative;
    transition: 300ms ease all;
    width: 20px;

    &:checked {
      border-color: ${PRIMARY};

      &:after {
        background: ${PRIMARY};
        border-radius: 50%;
        content: "";
        height: 10px;
        left: 0;
        margin: 0 auto;
        position: absolute;
        right: 0;
        top: 50%;
        transform: translateY(-50%);
        width: 10px;
      }
    }
  }
`;

export const QuestionWrapper = styled.section`
  align-items: center;
  ${({ correction }) => handleCorrectionStatus(correction)}
  display: flex;
  height: 100vh;
  justify-content: center;

  .wrong {
    background-color: ${WARNING_LIGHT};

    input[type="radio"] {
      border-color: ${WARNING};
      &::after {
        background-color: ${WARNING};
      }
    }
  }

  .correct {
    background-color: ${SUCCESS_LIGHT};

    input[type="radio"] {
      border-color: ${SUCCESS};
      &::after {
        background-color: ${SUCCESS};
      }
    }
  }
`;

export const QuestionContent = styled.div`
  border: 1px solid ${GREY};
  max-width: 90%;
  padding: 25px;
  width: 740px;
`;

export const QuestionHeader = styled.header`
  position: relative;
`;

export const QuestionInstitution = styled.h1`
  font-size: ${SIZES.lg};
  margin: 0 auto 20px;
`;

export const QuestionTitle = styled.div`
  font-size: ${SIZES.md};

  i {
    display: block;
    margin: 20px auto 0;
  }
`;

export const QuestionOptions = styled.form`
  padding: 40px 0;
`;

export const QuestionOptionsItemLetter = styled.span`
  font-weight: bold;
  padding: 0 10px 0 0;

  &::after {
    content: ".";
  }
`;

export const QuestionFooter = styled.footer`
  align-items: center;
  border-bottom: 1px solid ${GREY};
  border-top: 1px solid ${GREY};
  display: flex;
  padding: 20px;

  @media ${DEVICE.mobile} {
    display: block;
  }
`;

export const QuestionFooterResponse = styled.div`
  flex-shrink: 0;
  width: 50%;

  b {
    display: block;
  }

  @media ${DEVICE.mobile} {
    width: 100%;
    margin: 0 auto 20px;
  }
`;

export const QuestionFooterAction = styled.div`
  display: flex;
  flex-shrink: 0;
  justify-content: flex-end;
  width: 50%;

  @media ${DEVICE.mobile} {
    width: 100%;
  }
`;
