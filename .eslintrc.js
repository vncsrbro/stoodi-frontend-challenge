module.exports = {
  env: {
    browser: true,
    es6: true
  },
  extends: ["plugin:react/recommended", "airbnb", "prettier", "prettier/react"],
  globals: {
    Atomics: "readonly",
    SharedArrayBuffer: "readonly"
  },
  parser: "babel-eslint",
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 2018,
    sourceType: "module"
  },
  plugins: ["react", "prettier", "better-styled-components"],
  rules: {
    "prettier/prettier": "error",
    "react/jsx-filename-extension": ["warn", { extensions: [".js", ".jsx"] }],
    "import/prefer-default-export": "off",
    "react/state-in-constructor": "off",
    "react/jsx-props-no-spreading": 0,
    "better-styled-components/sort-declarations-alphabetically": 2,
    camelcase: [0],
    "dot-notation": [0],
    "default-case": ["error", { commentPattern: "^skip\\sdefault" }],
    "no-unused-vars": [
      "error",
      {
        varsIgnorePattern: "response"
      }
    ]
  }
};
